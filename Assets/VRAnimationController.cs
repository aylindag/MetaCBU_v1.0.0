using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class VRAnimationController : MonoBehaviour
{
    private Animator _animator;
    public InputActionProperty moveInputAction;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
        float moveValue = moveInputAction.action.ReadValue<float>();
        Vector2 input = moveInputAction.action.ReadValue<Vector2>();
        Debug.Log("tostring:"+moveInputAction.ToString());
        Debug.Log("input"+input);
        Debug.Log(moveValue);
        */
        
        var leftHandValue = moveInputAction.action?.ReadValue<Vector2>() ?? Vector2.zero;
        Debug.Log("Debug: " + leftHandValue);
        _animator.SetFloat("Speed", leftHandValue.x);
    }
}
