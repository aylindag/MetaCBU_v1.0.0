using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class ActivateTeleportationRay : MonoBehaviour
{

    public GameObject rightTeleportation;

    public InputActionProperty rightActivate;
    
    //Gun

    public InputActionProperty rightCansel;
    
    // Interactor

    public XRRayInteractor rightRay;
    public XRRayInteractor leftRay;

    // Update is called once per frame
    void Update()
    {
        bool isRightRayHovering = rightRay.TryGetHitInfo(out Vector3 rightPos, out Vector3 rightNormal, out int rightNumber,
            out bool rightValid);
        //rightTeleportation.SetActive(rightActivate.action.ReadValue<float>() > 0.1f);
        //rightTeleportation.SetActive(rightCansel.action.ReadValue<float>() == 0 && rightActivate.action.ReadValue<float>() > 0.1f);
        rightTeleportation.SetActive(!isRightRayHovering && rightCansel.action.ReadValue<float>() == 0 && rightActivate.action.ReadValue<float>() > 0.1f);
    }
}
