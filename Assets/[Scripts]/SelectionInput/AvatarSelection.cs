using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AvatarSelection : MonoBehaviour
{
    public static int avatar1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (avatar1 == 1 || avatar1 == 2 || avatar1 == 3 || avatar1 == 4)
        {
            SceneManager.LoadScene("[Scenes]/ChoseYourControl");
        }
    }

    public void SelectOne()
    {
        avatar1 = 1;
       
    }

    public void SelectTwo()
    {
        avatar1 = 2;
        
    }

    public void SelectTree()
    {
        avatar1 = 3;
    }

    public void SelectFour()
    {
        avatar1 = 4;
    }
}
