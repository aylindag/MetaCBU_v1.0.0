using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
  
    public class SelectionInput : MonoBehaviour
    {
        public static int selectionDesktop;
        public static int selectionVR ;

        // Update is called once per frame
        void Update()
        {
            //Değerlere göre sonraki sahneye gönderen kod
            if (selectionDesktop == 1)
            {
                Debug.Log(selectionDesktop);
                SceneManager.LoadScene("[Scenes]/PhotonLoaing");
            }
            if (selectionVR == 1)
            {
                SceneManager.LoadScene("[Scenes]/PhotonLoaing");
            }
        }
        // Hangi seçim yapıldıysa onun değerini 1 arttıran kod
        public void SelectedDesktop()
        {
            selectionDesktop = 1;
            selectionVR = 0;
        }

        public void SelectedVR()
        {
            selectionVR = 1;
            selectionDesktop = 0;
        }
    }


