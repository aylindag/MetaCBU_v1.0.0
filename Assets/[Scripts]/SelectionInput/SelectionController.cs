using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectionController : MonoBehaviour
{
    //private SelectionInput _selectionInput;
    private int desktop;
    private int vr;
    [SerializeField] private GameObject VRCam; 
    [SerializeField] private GameObject DesktopCam;
    private Component asd;

    private void Awake()
    {
        desktop = SelectionInput.selectionDesktop;
        vr = SelectionInput.selectionVR;
        //asd.gameObject.GetComponent<VRTransportPlayer>().enabled = false;
        VRCam.gameObject.GetComponent<VRTransportPlayer>().enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("A : "+desktop);
        Debug.Log("B : " +vr);
        //Eğer desktop seçildiyse hiarchy kısmında desktop kamarayı aktif eden kod
        if (desktop == 1)
        {
            VRCam.gameObject.GetComponent<VRTransportPlayer>().enabled = false;
            VRCam.SetActive(false);
            DesktopCam.SetActive(true);
            
            
            
        }
        //Eğer vr gözlük seçildiyse hiarchy kısmında vr kamarayı aktif eden kod
        else if (vr == 1)
        {
            DesktopCam.SetActive(false);
            VRCam.gameObject.GetComponent<VRTransportPlayer>().enabled = true;
            VRCam.SetActive(true);
            asd.gameObject.SetActive(true);
            

        }
        // Hata sonucu her iki değerin boş gelmesi olursa sahne seçimine geri atma
        /*else if (desktop == 0 && vr == 0)
        {
            SceneManager.LoadScene("ChoseYourControl");
        }
        */
    }

 
}
