using System;
using UnityEngine;
using Photon.Pun;
using UnityEngine.Video;
using UnityEngine.SceneManagement;



    public class TransportPlayer : MonoBehaviourPunCallbacks
    {
        private Component moveInput;
        //private Transform teleportTarget;
        public Transform thePlayer;
        public GameObject hideMain;
        public GameObject showMain;
        public GameObject hideB1;
        public GameObject showB1;
        public GameObject hideB2;
        public GameObject showB2;
        //public GameObject hideB3;
        //public GameObject showB3;
        public GameObject hideB4;
        public GameObject showB4;
        public GameObject hideB5;
        public GameObject showB5;
        public GameObject MainGTarget;
        public GameObject B1GTarget;
        public GameObject B2GTarget;
        public GameObject AmfiGTarget;
        public GameObject B4GTarget;
        public GameObject B5GTarget;
        public GameObject MainCTarget;
        public GameObject B1CTarget;
        public GameObject B2CTarget;
        public GameObject AmfiCTarget;
        public GameObject B4CTarget;
        public GameObject B5CTarget;
        private PhotonView _photon;
        
        
        
        public GameObject Hall_Target;
        public GameObject HallBuild;
        public GameObject Amfi;
        public GameObject HallCTarget;
        
        
        public GameObject ClassBuild;
        public GameObject ClassGTarget;
        public GameObject ClassCTarget;
        
        
        
        private VideoPlayer _videoPlayer1;
        private VideoPlayer _videoPlayer2;
        private VideoPlayer _videoPlayer3;
        private VideoPlayer _amfiVideoPlayer;
       
        
        //public GameObject Course_1;
        //public GameObject Course_1_Player;
        //public GameObject Course_2;
        //public GameObject Course_2_Player;
        //public GameObject Course_3;
        //public GameObject Course_3_Player;
        public GameObject AmfiVideo;

        private GameObject newScene;
        
        private void Awake()
        {
            _photon = GetComponent<PhotonView>();
        }

        private void Start()
        {
            hideMain = GameObject.Find("Empty_Main");
            showMain = GameObject.Find("Full_Main");
            MainGTarget = GameObject.Find("Main_Giris_Target");
            MainCTarget = GameObject.Find("Main_Cikis_Target");
            showMain.gameObject.SetActive(false);
            
            hideB1 = GameObject.Find("Empty_B1");
            showB1 = GameObject.Find("Full_B1");
            B1GTarget = GameObject.Find("B1_Giris_Target");
            B1CTarget = GameObject.Find("B1_Cikis_Target");
            showB1.gameObject.SetActive(false);
            
            hideB2 = GameObject.Find("Empty_B2");
            showB2 = GameObject.Find("Full_B2");
            B2GTarget = GameObject.Find("B2_Giris_Target");
            B2CTarget = GameObject.Find("B2_Cikis_Target");
            showB2.gameObject.SetActive(false);
            
            //hideB3 = GameObject.Find("Empty_B3");
            //showB3 = GameObject.Find("Full_B3");
            AmfiGTarget = GameObject.Find("AmfiGTarget");
            AmfiCTarget = GameObject.Find("AmfiCikisTarget");
            //showB3.gameObject.SetActive(false);
            Hall_Target = GameObject.Find("HallGTarget");
            HallBuild = GameObject.Find("SDKM_Hall");
            Amfi = GameObject.Find("Amfi");
            HallCTarget = GameObject.Find("AmfiCTarget");

            AmfiVideo = GameObject.Find("AmfiVideoPlayer");
            
            
            Amfi.gameObject.SetActive(false);
            HallBuild.gameObject.SetActive(false);
            
            hideB4 = GameObject.Find("Empty_B4");
            showB4 = GameObject.Find("Full_B4");
            B4GTarget = GameObject.Find("B4_Giris_Target");
            B4CTarget = GameObject.Find("B4_Cikis_Target");
            showB4.gameObject.SetActive(false);
            
            hideB5 = GameObject.Find("Empty_B5");
            showB5 = GameObject.Find("Full_B5");
            B5GTarget = GameObject.Find("B5_Giris_Target");
            B5CTarget = GameObject.Find("B5_Cikis_Target");
            showB5.gameObject.SetActive(false);
            
            
            ClassBuild = GameObject.Find("Class");
            ClassCTarget = GameObject.Find("Class_Cikis_Target");
            ClassGTarget = GameObject.Find("Class_Giris_Target");
            ClassBuild.gameObject.SetActive(false);
            
            
            //_videoPlayer1 = Course_1_Player.GetComponent<VideoPlayer>();
            //_videoPlayer2 = Course_2_Player.GetComponent<VideoPlayer>();
            //_videoPlayer3 = Course_3_Player.GetComponent<VideoPlayer>();
            _amfiVideoPlayer = AmfiVideo.GetComponent<VideoPlayer>();
            
            
            newScene = GameObject.FindGameObjectWithTag("NewScene");
        }
        


        // GİRİŞ FONKSİYONLARI
        public void Giris_Main()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = MainGTarget.transform.position;
            hideMain.gameObject.SetActive(false);
            showMain.gameObject.SetActive(true);
        }
        public void Giris_B1()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B1GTarget.transform.position;
            hideB1.gameObject.SetActive(false);
            showB1.gameObject.SetActive(true);
        }
        public void Giris_B2()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B2GTarget.transform.position;
            hideB2.gameObject.SetActive(false);
            showB2.gameObject.SetActive(true);
        }
        public void SDKMGiris()
        {
            HallBuild.gameObject.SetActive(true);  
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = Hall_Target.transform.position;
            //hideB3.gameObject.SetActive(false);
            //showB3.gameObject.SetActive(true);
            //thePlayer.gameObject.SetActive(false);
            
        }

        void AmfiGiris()
        {
            /*
            Amfi.gameObject.SetActive(true);
            transform.position = AmfiGTarget.transform.position;
            HallBuild.gameObject.SetActive(false);
            */
            
            _amfiVideoPlayer.playOnAwake = true;
            Amfi.gameObject.SetActive(true);
            transform.position = AmfiGTarget.transform.position;
            HallBuild.gameObject.SetActive(false);
        }
        
        public void Giris_B4()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B4GTarget.transform.position;
            hideB4.gameObject.SetActive(false);
            showB4.gameObject.SetActive(true);
            // transform.position = new Vector3(7f, -180.6896f, -137.86f);

        }

        public void Giris_B5()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B5GTarget.transform.position;
            hideB5.gameObject.SetActive(false);
            showB5.gameObject.SetActive(true);
        }
        
        public void ClassGiris()
        {
            ClassBuild.gameObject.SetActive(true);
            transform.position = ClassGTarget.transform.position;
        }
        
        // SAHNE GEÇİŞ FONKSİYONU

        public void New_Scene()
        {
                PhotonNetwork.LeaveRoom();
                Debug.Log("Yeni Sahneye Geçiş Başlıyor");
                SceneManager.LoadScene("[Scenes]/SIUScene");
            
        }
        //SIU DAN MANİN SAHNEYE DÖNÜŞ

        void BackToMain()
        {
            PhotonNetwork.LeaveRoom();
            Debug.Log("Yeni Sahneye Geçiş Başlıyor");
            SceneManager.LoadScene("[Scenes]/PhotonLoaing");
        }

        // ÇIKIŞ FONKSİYONLARI
        public void Cikis_Main()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = MainCTarget.transform.position;
            hideMain.gameObject.SetActive(true);
            showMain.gameObject.SetActive(false);
        }
        public void Cikis_B1()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B1CTarget.transform.position;
            hideB1.gameObject.SetActive(true);
            showB1.gameObject.SetActive(false);
        }
        public void Cikis_B2()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B2CTarget.transform.position;
            hideB2.gameObject.SetActive(true);
            showB2.gameObject.SetActive(false);
        }
        public void Cikis_B3()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = AmfiCTarget.transform.position;
            //hideB3.gameObject.SetActive(true);
            //showB3.gameObject.SetActive(false);
        }
        public void Cikis_B4()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B4CTarget.transform.position;
            hideB4.gameObject.SetActive(true);
            showB4.gameObject.SetActive(false);
        }

        public void Cikis_B5()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B5CTarget.transform.position;
            hideB5.gameObject.SetActive(true);
            showB5.gameObject.SetActive(false);
        }

        void HallCikis()
        {
            transform.position = HallCTarget.transform.position;
            HallBuild.gameObject.SetActive(false);
        }

        void AmfiCikis()
        {
            HallBuild.gameObject.SetActive(true);
            transform.position = Hall_Target.transform.position;
            Amfi.gameObject.SetActive(false);
        }
        
        public void ClassCikis()
        {
            transform.position = ClassCTarget.transform.position;
            ClassBuild.gameObject.SetActive(false);
        }
        
        // GİRİŞ TRIGGER
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Main_Giris") && _photon.IsMine)
            {
                Debug.Log("B1 giriş calıştı");
                Giris_Main();
            }
            
            else if (other.gameObject.CompareTag("B1_Giris") && _photon.IsMine)
            {
                Debug.Log("B1 giriş calıştı");
                Giris_B1();
            }
            else if (other.gameObject.CompareTag("B2_Giris") && _photon.IsMine)
            {
                Debug.Log("B2 giriş calıştı");
                Giris_B2();
            }
            
            else if (other.gameObject.CompareTag("KulturMGiris") && _photon.IsMine)
            {
                Debug.Log("B3 giris calıştı");
                SDKMGiris();
            }
            else if (other.gameObject.CompareTag("B4_Giris") && _photon.IsMine)
            {
                Debug.Log("B4 giriş çalıştı");
                Giris_B4();
            }
            
            else if (other.gameObject.CompareTag("B5_Giris") && _photon.IsMine)
            {
                Debug.Log("B5 giriş calıştı");
                Giris_B5();
            }
 
            else if (other.gameObject.CompareTag("Main_Cikis") && _photon.IsMine)
            {
                Debug.Log("Bu sefer trigger çalıştı be");
                Cikis_Main();
            }
            
            else if (other.gameObject.CompareTag("B1_Cikis") && _photon.IsMine)
            {
                Debug.Log("Bu sefer trigger çalıştı be");
                Cikis_B1();
            }
            else if (other.gameObject.CompareTag("B2_Cikis") && _photon.IsMine)
            {
                Debug.Log("Bu sefer trigger çalıştı be");
                Cikis_B2();
            }
            
            else if (other.gameObject.CompareTag("B3_Cikis") && _photon.IsMine)
            {
                Debug.Log("Bu sefer trigger çalıştı be");
                Cikis_B3();
            }
            else if (other.gameObject.CompareTag("B4_Cikis") && _photon.IsMine)
            {
                Debug.Log("Bu sefer trigger çalıştı be");
                Cikis_B4();
            }
            
            else if (other.gameObject.CompareTag("B5_Cikis") && _photon.IsMine)
            {
                Debug.Log("Bu sefer trigger çalıştı be");
                Cikis_B5();
            }else if (other.gameObject.CompareTag("Amfi_Giris"))
            {
                AmfiGiris();
            }else if (other.gameObject.CompareTag("Hall_Cikis"))
            {
                HallCikis();
            }else if (other.gameObject.CompareTag("Amfi_Cikis"))
            {
                AmfiCikis();
            }else if (other.gameObject.CompareTag("Class_1Giris"))
            {
                ClassGiris();
            }else if (other.gameObject.CompareTag("Class_Cikis"))
            {
                ClassCikis();
            }else if (other.gameObject.CompareTag("NewScene"))
            {
                New_Scene();
            }else if (other.gameObject.CompareTag("Back_Main"))
            {
                BackToMain();
            }
        }
        
        
      
    }

