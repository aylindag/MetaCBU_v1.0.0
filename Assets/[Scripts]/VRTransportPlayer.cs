using System;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.XR.Interaction.Toolkit;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;


public class VRTransportPlayer : MonoBehaviour
    {
        public GameObject thePlayer;
        public GameObject photonAvatar;
        public GameObject hideMain;
        public GameObject showMain;
        public GameObject hideB1;
        public GameObject showB1;
        public GameObject hideB2;
        public GameObject showB2;
        //public GameObject hideB3;
        //public GameObject showB3;
        public GameObject hideB4;
        public GameObject showB4;
        public GameObject hideB5;
        public GameObject showB5;
        public GameObject show360;
        public GameObject amfi;
        [SerializeField] private GameObject developerTeam;


        public GameObject MainGTarget;
        public GameObject B1GTarget;
        public GameObject B2GTarget;
        //public GameObject B3GTarget;
        public GameObject B4GTarget;
        public GameObject B5GTarget;
        public GameObject MainCTarget;
        public GameObject B1CTarget;
        public GameObject B2CTarget;
       // public GameObject B3CTarget;
        public GameObject B4CTarget;
        public GameObject B5CTarget;

        public GameObject AmfiGTarget;
        public GameObject AmfiCTarget;
        public GameObject Hall_Target;
        public GameObject HallBuild;

        public GameObject ClassBuild;
        public GameObject ClassGTarget;
        public GameObject ClassCTarget;
        
        public GameObject Course_1;
        public GameObject Course_1_Player;
        public GameObject Course_2;
        public GameObject Course_2_Player;
        public GameObject Course_3;
        public GameObject Course_3_Player;
        public GameObject AmfiVideo;

        public GameObject VR360Player;
        public GameObject VR360Video;


        private VideoPlayer _videoPlayer1;
        private VideoPlayer _videoPlayer2;
        private VideoPlayer _videoPlayer3;
        private VideoPlayer _amfiVideoPlayer;
        private VideoPlayer _VR360VideoPlayer;
        
        
        [SerializeField] private GameObject leftHand;
        [SerializeField] private GameObject rightHand;
        
        //Components
        private Component _move;
        private Component _teleport;
        private Component _teleportRay;

        private GameObject newScene;


        private void Start()
        {
            MainGTarget = GameObject.Find("Main_Giris_Target");
            MainCTarget = GameObject.Find("Main_Cikis_Target");
            
            B1GTarget = GameObject.Find("B1_Giris_Target");
            B1CTarget = GameObject.Find("B1_Cikis_Target");
            
            B2GTarget = GameObject.Find("B2_Giris_Target");
            B2CTarget = GameObject.Find("B2_Cikis_Target");
            
            //B3GTarget = GameObject.Find("B3_Giris_Target");
            //B3CTarget = GameObject.Find("B3_Cikis_Target");
            
            B4GTarget = GameObject.Find("B4_Giris_Target");
            B4CTarget = GameObject.Find("B4_Cikis_Target");
            
            B5GTarget = GameObject.Find("B5_Giris_Target");
            B5CTarget = GameObject.Find("B5_Cikis_Target");
            
            
            hideMain = GameObject.Find("Empty_Main");
            showMain = GameObject.Find("Full_Main");
            
            showMain.gameObject.SetActive(false);
            
            hideB1 = GameObject.Find("Empty_B1");
            showB1 = GameObject.Find("Full_B1");
           
            showB1.gameObject.SetActive(false);
            
            hideB2 = GameObject.Find("Empty_B2");
            showB2 = GameObject.Find("Full_B2");
            
            showB2.gameObject.SetActive(false);
            
            
            //kulturSalon = GameObject.Find("Amfi");
            //XRSDKMPlayer = GameObject.Find("XRAmfiPlayer");
            
           
            //showB3.gameObject.SetActive(false);
            //kulturSalon.gameObject.SetActive(false);
            
            
            hideB4 = GameObject.Find("Empty_B4");
            showB4 = GameObject.Find("Full_B4");
           
            showB4.gameObject.SetActive(false);
            
            hideB5 = GameObject.Find("Empty_B5");
            showB5 = GameObject.Find("Full_B5");
            
            showB5.gameObject.SetActive(false);
            show360.gameObject.SetActive(false);
            HallBuild.gameObject.SetActive(false);
            amfi.gameObject.SetActive(false);
            developerTeam.gameObject.SetActive(false);

            _videoPlayer1 = Course_1_Player.GetComponent<VideoPlayer>();
            _videoPlayer2 = Course_2_Player.GetComponent<VideoPlayer>();
            _videoPlayer3 = Course_3_Player.GetComponent<VideoPlayer>();
            _amfiVideoPlayer = AmfiVideo.GetComponent<VideoPlayer>();
            _VR360VideoPlayer = VR360Video.GetComponent<VideoPlayer>();
            
            
            _move = thePlayer.GetComponent<ActionBasedContinuousMoveProvider>();
            _teleport = thePlayer.GetComponent<TeleportationProvider>();
            _teleportRay = thePlayer.GetComponent<ActivateTeleportationRay>();
   
            newScene = GameObject.FindGameObjectWithTag("NewScene");

        }

        private void Update()
        {
            //vrAvatar = GameObject.FindGameObjectWithTag("VRPlayer");
            if (photonAvatar == null)
            {
                photonAvatar = GameObject.FindGameObjectWithTag("VRPlayer");
            }
        }


        // GİRİŞ FONKSİYONLARI
        public void Giris_Main()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = MainGTarget.transform.position;
            hideMain.gameObject.SetActive(false);
            showMain.gameObject.SetActive(true);
        }
        public void Giris_B1()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B1GTarget.transform.position;
            hideB1.gameObject.SetActive(false);
            showB1.gameObject.SetActive(true);
        }
        public void Giris_B2()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = ClassCTarget.transform.position;
            hideB2.gameObject.SetActive(false);
            showB2.gameObject.SetActive(true);
        }
        public void AmfiGiris()
        {
            _amfiVideoPlayer.playOnAwake = true;
            amfi.gameObject.SetActive(true);
            transform.position = AmfiGTarget.transform.position;
            HallBuild.gameObject.SetActive(false);
            
            
            
            
            //thePlayer.gameObject.SetActive(false);
            //vrAvatar.gameObject.SetActive(false);
            //amfi.gameObject.SetActive(true);
            Debug.Log("B3 Giriş");
            
        }

        public void HallGiris()
        {
            Debug.Log("Hall Girisçalıştı");
            HallBuild.gameObject.SetActive(true);
            transform.position = Hall_Target.transform.position;
            
        }

        public void ClassGiris()
        {
            ClassBuild.gameObject.SetActive(true);
            transform.position = ClassGTarget.transform.position;
        }
        public void Giris_B4()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B4GTarget.transform.position;
            hideB4.gameObject.SetActive(false);
            showB4.gameObject.SetActive(true);
            // transform.position = new Vector3(7f, -180.6896f, -137.86f);

        }

        public void Giris_B5()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B5GTarget.transform.position;
            hideB5.gameObject.SetActive(false);
            showB5.gameObject.SetActive(true);
        }

        public void Course1Giris()
        {
            ClassBuild.gameObject.SetActive(true);
            transform.position = ClassGTarget.transform.position;
            showB2.gameObject.SetActive(false);
            hideB2.gameObject.SetActive(true);
            _videoPlayer1.playOnAwake = true;
            //Course_2_Player.gameObject.SetActive(false);
            //Course_3_Player.gameObject.SetActive(false);
            Course_1.gameObject.SetActive(true);
            Course_2.gameObject.SetActive(false);
            Course_3.gameObject.SetActive(false);
            
            Debug.Log("Course1Giris Çalıştı");
        }

        public void Course2Giris()
        {
            ClassBuild.gameObject.SetActive(true);
            transform.position = ClassGTarget.transform.position;
            showB2.gameObject.SetActive(false);
            hideB2.gameObject.SetActive(true);
            _videoPlayer2.playOnAwake = true;
            //Course_2_Player.gameObject.SetActive(true);
            //Course_3_Player.gameObject.SetActive(false);
            Course_1.gameObject.SetActive(false);
            Course_2.gameObject.SetActive(true);
            Course_3.gameObject.SetActive(false);
        }

        public void Course3Giris()
        {
            ClassBuild.gameObject.SetActive(true);
            transform.position = ClassGTarget.transform.position;
            showB2.gameObject.SetActive(false);
            hideB2.gameObject.SetActive(true);
            _videoPlayer3.playOnAwake = true;
            //Course_2_Player.gameObject.SetActive(false);
            //Course_3_Player.gameObject.SetActive(true);
            Course_1.gameObject.SetActive(false);
            Course_3.gameObject.SetActive(true);
            Course_2.gameObject.SetActive(false);
        }
        
        public void Giris360()
        {
            thePlayer.gameObject.SetActive(false);
            
        }
        
        // SAHNE GEÇİŞ FONKSİYONU

        public void SIUScene()
        {
            PhotonNetwork.LeaveRoom();
            Debug.Log("Yeni Sahneye Geçiş Başlıyor");
            SceneManager.LoadScene("[Scenes]/SIUScene");
            
        }
        
        // MAİN SAHNE GEÇİŞ
        
        void BackToMain()
        {
            PhotonNetwork.LeaveRoom();
            Debug.Log("Yeni Sahneye Geçiş Başlıyor");
            SceneManager.LoadScene("[Scenes]/PhotonLoaing");
        }

        // ÇIKIŞ FONKSİYONLARI
        public void Cikis_Main()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = MainCTarget.transform.position;
            hideMain.gameObject.SetActive(true);
            showMain.gameObject.SetActive(false);
        }
        public void Cikis_B1()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B1CTarget.transform.position;
            hideB1.gameObject.SetActive(true);
            showB1.gameObject.SetActive(false);
        }
        public void Cikis_B2()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B2CTarget.transform.position;
            hideB2.gameObject.SetActive(true);
            showB2.gameObject.SetActive(false);
        }

        public void HallCikis()
        {
            transform.position = AmfiCTarget.transform.position;
            HallBuild.gameObject.SetActive(false);
            //developerTeam.gameObject.SetActive(true);
        }
        public void AmfiCikis()
        {
            //vrAvatar.gameObject.SetActive(true);
            HallBuild.gameObject.SetActive(true);
            transform.position = Hall_Target.transform.position;
            amfi.gameObject.SetActive(false);
            //vrAvatar.gameObject.SetActive(true);
            //thePlayer.gameObject.SetActive(true);
            _amfiVideoPlayer.playOnAwake = true;
           
       
            Debug.Log("B3 Çıkış");
        }

        public void ClassCikis()
        {
            showB2.gameObject.SetActive(true);
            hideB2.gameObject.SetActive(false);
            transform.position = ClassCTarget.transform.position;
            Course_1.gameObject.SetActive(false);
            ClassBuild.gameObject.SetActive(false);
        }
        public void Cikis_B4()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B4CTarget.transform.position;
            hideB4.gameObject.SetActive(true);
            showB4.gameObject.SetActive(false);
        }

        public void Cikis_B5()
        {
            Debug.Log("Fonksiyon Çalıştı");
            transform.position = B5CTarget.transform.position;
            hideB5.gameObject.SetActive(true);
            showB5.gameObject.SetActive(false);
        }

        public void VideoCikis()
        {
            //vrPlayer.SetActive(false);
            thePlayer.SetActive(true);
            //transform.position = B3CTarget.transform.position;
            show360.SetActive(false);
            
        }
        public void VRVideoEnter()
        {
            //thePlayer.gameObject.SetActive(false);
            //photonAvatar.gameObject.SetActive(false);
            //_VR360VideoPlayer.playOnAwake = true;
            //show360.gameObject.SetActive(true);
            //VR360Player.gameObject.SetActive(true);
            
            
            _VR360VideoPlayer.playOnAwake = true;
            photonAvatar.gameObject.SetActive(false);
            show360.gameObject.SetActive(true);
            rightHand.gameObject.SetActive(true);
            leftHand.gameObject.SetActive(true);
            
            thePlayer.transform.position = VR360Player.transform.position;
            _move.GetComponent<ActionBasedContinuousMoveProvider>().enabled = false;
            _teleport.GetComponent<TeleportationProvider>().enabled = false;
            _teleportRay.GetComponent<ActivateTeleportationRay>().enabled = false;
        }

        public void VRVideoExit()
        {
            thePlayer.transform.position = ClassCTarget.transform.position;
            photonAvatar.gameObject.SetActive(true);
            show360.gameObject.SetActive(false);
            rightHand.gameObject.SetActive(false);
            leftHand.gameObject.SetActive(false);
            
            
            _move.GetComponent<ActionBasedContinuousMoveProvider>().enabled = true;
            _teleport.GetComponent<TeleportationProvider>().enabled = true;
            _teleportRay.GetComponent<ActivateTeleportationRay>().enabled = true;
            _VR360VideoPlayer.playOnAwake = true;
        }
        
        // GİRİŞ TRIGGER
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Main_Giris") )
            {
                Debug.Log("B1 giriş calıştı");
                Giris_Main();
            }
            
            else if (other.gameObject.CompareTag("B1_Giris") )
            {
                Debug.Log("B1 giriş calıştı");
                Giris_B1();
            }
            else if (other.gameObject.CompareTag("B2_Giris"))
            {
                Debug.Log("B2 giriş calıştı");
                Giris_B2();
            }
            
            else if (other.gameObject.CompareTag("360VR_Giris") )
            {
                Debug.Log("360VR_Giris Calıstı");
                //Giris_B3();
            }
            else if (other.gameObject.CompareTag("B4_Giris") )
            {
                Debug.Log("B4 giriş çalıştı");
                Giris_B4();
            }
            
            else if (other.gameObject.CompareTag("B5_Giris") )
            {
                Debug.Log("B5 giriş calıştı");
                Giris_B5();
            }
 
            else if (other.gameObject.CompareTag("Main_Cikis") )
            {
                Debug.Log("Bu sefer trigger çalıştı be");
                Cikis_Main();
            }
            
            else if (other.gameObject.CompareTag("B1_Cikis") )
            {
                Debug.Log("Bu sefer trigger çalıştı be");
                Cikis_B1();
            }
            else if (other.gameObject.CompareTag("B2_Cikis") )
            {
                Debug.Log("Bu sefer trigger çalıştı be");
                Cikis_B2();
            }
            
            else if (other.gameObject.CompareTag("Amfi_Cikis"))
            {
                Debug.Log("Bu sefer trigger çalıştı be");
                AmfiCikis();
            }
            else if (other.gameObject.CompareTag("B4_Cikis"))
            {
                Debug.Log("Bu sefer trigger çalıştı be");
                Cikis_B4();
            }
            
            else if (other.gameObject.CompareTag("B5_Cikis"))
            {
                Debug.Log("Bu sefer trigger çalıştı be");
                Cikis_B5();
            }
            else if (other.gameObject.CompareTag("KulturMGiris"))
            {
                HallGiris();
            }else if (other.gameObject.CompareTag("Hall_Cikis"))
            {
                HallCikis();
            }else if (other.gameObject.CompareTag("Amfi_Giris"))
            {
                AmfiGiris();
            }
            else if (other.gameObject.CompareTag("Class_Cikis"))
            {
                ClassCikis();
            }
            else if (other.gameObject.CompareTag("NewScene"))
            {
                SIUScene();
            }else if (other.gameObject.CompareTag("Back_Main"))
            {
                BackToMain();
            }
        }
        
        
      
    }

