﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CameraSelection : MonoBehaviour
{
    public UnityEvent cameraSelectionEvent = new UnityEvent();

    private void Start()
    {
        cameraSelectionEvent.Invoke();
    }
}