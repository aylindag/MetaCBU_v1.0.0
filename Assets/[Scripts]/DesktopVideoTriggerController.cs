using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Photon.Pun;
using StarterAssets;
using Unity.VideoHelper;
using Unity.VisualScripting;
using Unity.XR.CoreUtils;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class DesktopVideoTriggerController : MonoBehaviour
{
    private Component moveInput;
    public GameObject videoCanvas;
    public GameObject steamVRCam;
    private bool _isTrigger = false;
    public GameObject desktopCam;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_isTrigger && Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("E Basıldı");
            moveInput.gameObject.GetComponent<PlayerInput>().enabled = false;
            steamVRCam.SetActive(true);
            videoCanvas.SetActive(false);
            desktopCam.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            videoCanvas.SetActive(true);   
            moveInput = other.gameObject.GetComponent<PlayerInput>();
            _isTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            videoCanvas.SetActive(false);
            _isTrigger = false;
            desktopCam.SetActive(true);
        }
    }

    public void exitMedot()
    {
        steamVRCam.SetActive(false);
        desktopCam.SetActive(true);
        moveInput.gameObject.GetComponent<PlayerInput>().enabled = true;
        _isTrigger = false;
    }
}
