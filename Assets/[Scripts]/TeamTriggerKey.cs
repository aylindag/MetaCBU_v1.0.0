using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamTriggerKey : MonoBehaviour
{
    
    public GameObject Team;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.O))
        {
            Debug.Log("O Basıldı");
            Team.SetActive(true);
        }
        
        else if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("P Basıldı");
            Team.SetActive(false);
        }
    }
}
