using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnAnimation : MonoBehaviour
{
    [SerializeField] private float speedX;
    [SerializeField] private float speedY;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(360*speedX*Time.deltaTime,360*speedY*Time.deltaTime,0f);
    }
}
