using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Photon.Pun;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace VRPlayerBookReadController
{
    public class VRPlayerBookReadController : MonoBehaviour
    {
        public GameObject xrOrigin;
        public GameObject photonAvatar;
        public GameObject ReadCanvaso;
        public GameObject ReadCanvasv;
        public GameObject ReadCanvasd;
        private bool _photonServer;
        [SerializeField] private GameObject ReadPlayerO;
        [SerializeField] private GameObject leftHand;
        [SerializeField] private GameObject rightHand;
      
        //Components
        private Component _move;
        private Component _teleport;
        private Component _teleportRay;
        
        //Teleport Targets
        [SerializeField] private GameObject libraryTarget;

        private void Start()
        {
            _move = xrOrigin.GetComponent<ActionBasedContinuousMoveProvider>();
            _teleport = xrOrigin.GetComponent<TeleportationProvider>();
            _teleportRay = xrOrigin.GetComponent<ActivateTeleportationRay>();
        }

        private void Update()
        {
            //Avatar Check
            if (photonAvatar == null)
            {
                photonAvatar = GameObject.FindGameObjectWithTag("VRPlayer");
            }

        }


        // ÇIKIŞ FONKSİYONLARI
     
        public void CikisO()
        {
            xrOrigin.transform.position = libraryTarget.gameObject.transform.position;
            rightHand.gameObject.SetActive(false);
            leftHand.gameObject.SetActive(false);
            ReadCanvaso.gameObject.SetActive(false);
            _move.GetComponent<ActionBasedContinuousMoveProvider>().enabled = true;
            _teleport.GetComponent<TeleportationProvider>().enabled = true;
            _teleportRay.GetComponent<ActivateTeleportationRay>().enabled = true;
            photonAvatar.gameObject.SetActive(true);
 
        }

        public void CikisD()
        {
            //ReadPlayerO.gameObject.SetActive(false);
            xrOrigin.transform.position = libraryTarget.gameObject.transform.position;
            rightHand.gameObject.SetActive(false);
            leftHand.gameObject.SetActive(false);
            ReadCanvasd.gameObject.SetActive(false);
            _move.GetComponent<ActionBasedContinuousMoveProvider>().enabled = true;
            _teleport.GetComponent<TeleportationProvider>().enabled = true;
            _teleportRay.GetComponent<ActivateTeleportationRay>().enabled = true;
            
            //xrOrigin.gameObject.SetActive(true);
            photonAvatar.gameObject.SetActive(true);
        }

        public void CikisV()
        {
            xrOrigin.transform.position = libraryTarget.gameObject.transform.position;
            rightHand.gameObject.SetActive(false);
            leftHand.gameObject.SetActive(false);
            ReadCanvasv.gameObject.SetActive(false);
            //_move.GetComponent<ActionBasedContinuousMoveProvider>().enabled = true;
            _teleport.GetComponent<TeleportationProvider>().enabled = true;
            _teleportRay.GetComponent<ActivateTeleportationRay>().enabled = true;
            photonAvatar.gameObject.SetActive(true);
        }

        public void oCancas()
        {
            photonAvatar.gameObject.SetActive(false);
            ReadCanvaso.gameObject.SetActive(true);
            rightHand.gameObject.SetActive(true);
            leftHand.gameObject.SetActive(true);
            xrOrigin.transform.position = ReadPlayerO.transform.position;
            //_move.GetComponent<ActionBasedContinuousMoveProvider>().enabled = false;
            _teleport.GetComponent<TeleportationProvider>().enabled = false;
            _teleportRay.GetComponent<ActivateTeleportationRay>().enabled = false;
        }

        public void dCanvas()
        {
            //xrOrigin.gameObject.SetActive(false);
            photonAvatar.gameObject.SetActive(false);
            ReadCanvasd.gameObject.SetActive(true);
            rightHand.gameObject.SetActive(true);
            leftHand.gameObject.SetActive(true);
            xrOrigin.transform.position = ReadPlayerO.transform.position;
            //_move.GetComponent<ActionBasedContinuousMoveProvider>().enabled = false;
            _teleport.GetComponent<TeleportationProvider>().enabled = false;
            _teleportRay.GetComponent<ActivateTeleportationRay>().enabled = false;
            //ReadPlayerO.gameObject.SetActive(true);
        }

        public void vCanvas()
        {
            photonAvatar.gameObject.SetActive(false);
            ReadCanvasv.gameObject.SetActive(true);
            rightHand.gameObject.SetActive(true);
            leftHand.gameObject.SetActive(true);
            xrOrigin.transform.position = ReadPlayerO.transform.position;
            //_move.GetComponent<ActionBasedContinuousMoveProvider>().enabled = false;
            _teleport.GetComponent<TeleportationProvider>().enabled = false;
            _teleportRay.GetComponent<ActivateTeleportationRay>().enabled = false;
        }

    }
}

