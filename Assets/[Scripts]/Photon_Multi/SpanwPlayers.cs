using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;

public class SpanwPlayers : MonoBehaviour
{
    public GameObject playerPrefab1;
    public GameObject playerPrefab2;
    public GameObject playerPrefab3;
    public GameObject playerPrefab4;

    private int avatar1;
    

    public float minX;
    public float maxX;
    public float minZ;
    public float maxZ;

    private void Awake()
    {
        avatar1 = AvatarSelection.avatar1;
    }

    private void Start()
    {
        
        Vector2 randomPosition = new Vector2(Random.Range(minX, maxX), Random.Range(minZ, maxZ));
        PhotonNetwork.Instantiate(playerPrefab1.name, randomPosition, Quaternion.identity);
        if (avatar1 == 1)
        {
            Debug.Log("Avatar Değer: "+avatar1);
            
            //PhotonNetwork.Instantiate(playerPrefab1.name, randomPosition, Quaternion.identity); 
        }
        else if (avatar1 == 2)
        {
            Debug.Log("Avatar Değer: "+avatar1);
            
            PhotonNetwork.Instantiate(playerPrefab2.name, randomPosition, Quaternion.identity);
        }
        else if (avatar1 == 3)
        {
            
            PhotonNetwork.Instantiate(playerPrefab3.name, randomPosition, Quaternion.identity);
        }else if (avatar1 == 4)
        {
           
            PhotonNetwork.Instantiate(playerPrefab4.name, randomPosition, Quaternion.identity);
        }
        else
        {
            //SceneManager.LoadScene("[Scenes]/AvatarSelect");
        }

    }
}
