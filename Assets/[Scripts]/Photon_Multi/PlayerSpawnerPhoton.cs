using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class PlayerSpawnerPhoton : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject PlayerVR1;
    [SerializeField] private GameObject PlayerVR2;
    [SerializeField] private GameObject PlayerVR3;
    [SerializeField] private GameObject PlayerVR4;
    [SerializeField] private GameObject Player1;
    [SerializeField] private GameObject Player2;
    [SerializeField] private GameObject Player3;
    [SerializeField] private GameObject Player4;
    //[SerializeField] private GameObject VideoPlayer1;

    public Vector3 spanwPosition;

    private int selectedPlayer;

    private int desktop;
    private int vr;
    private void Start()
    {
        selectedPlayer = AvatarSelection.avatar1;
        desktop = SelectionInput.selectionDesktop;
        vr = SelectionInput.selectionVR;
    }

    public override void OnJoinedRoom()
    {
        if (desktop == 1)
        {
            if (selectedPlayer == 1)
            {
                Debug.Log("Odaya Bağlanıldı");
                Debug.Log("Karakter Oluşturuluyor");
                PhotonNetwork.Instantiate(Player1.name, spanwPosition, Quaternion.identity, 0, null); 
            }else if (selectedPlayer == 2)
            {
                Debug.Log("Odaya Bağlanıldı");
                Debug.Log("Karakter Oluşturuluyor");
                PhotonNetwork.Instantiate(Player2.name, spanwPosition, Quaternion.identity, 0, null);
            }else if (selectedPlayer == 3)
            {
                Debug.Log("Odaya Bağlanıldı");
                Debug.Log("Karakter Oluşturuluyor");
                PhotonNetwork.Instantiate(Player3.name, spanwPosition, Quaternion.identity, 0, null);
            
            }else if (selectedPlayer == 4)
            {
                Debug.Log("Odaya Bağlanıldı");
                Debug.Log("Karakter Oluşturuluyor");
                PhotonNetwork.Instantiate(Player4.name, spanwPosition, Quaternion.identity, 0, null);
            }
        }else if (vr == 1)
        {
            if (selectedPlayer == 1)
            { 
                Debug.Log("Odaya Bağlanıldı");
                Debug.Log("Karakter Oluşturuluyor");
                PhotonNetwork.Instantiate(PlayerVR1.name, spanwPosition, Quaternion.identity, 0, null);

            }
            else if (selectedPlayer == 2)
            { 
                Debug.Log("Odaya Bağlanıldı");
                Debug.Log("Karakter Oluşturuluyor");
                PhotonNetwork.Instantiate(PlayerVR2.name, spanwPosition, Quaternion.identity, 0, null);

            }
            else if (selectedPlayer == 3)
            { 
                Debug.Log("Odaya Bağlanıldı");
                Debug.Log("Karakter Oluşturuluyor");
                PhotonNetwork.Instantiate(PlayerVR3.name, spanwPosition, Quaternion.identity, 0, null);

            }
            else if (selectedPlayer == 4)
            { 
                Debug.Log("Odaya Bağlanıldı");
                Debug.Log("Karakter Oluşturuluyor");
                PhotonNetwork.Instantiate(PlayerVR4.name, spanwPosition, Quaternion.identity, 0, null);

            }

        }
        

        /*
        base.OnJoinedRoom();
        Debug.Log("Odaya Bağlanıldı");
        Debug.Log("Karakter Oluşturuluyor");
        PhotonNetwork.Instantiate(Player.name, new Vector3(-10, 5, -5), Quaternion.identity, 0, null);
        */
    }
}
