using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class ServerManager : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {
        //PhotonNetwork.NetworkingClient.AppId = PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime;
        //PhotonNetwork.ConnectToRegion("us");
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("asd");
        PhotonNetwork.JoinLobby();
    }
    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        Debug.Log("Lobiye Bağlanıldı");
        Debug.Log("Odaya Bağlanılıyor");
        PhotonNetwork.JoinOrCreateRoom("OdaIsmi", new RoomOptions {MaxPlayers = 10, IsOpen = true, IsVisible = true},
            TypedLobby.Default);
        SceneManager.LoadScene("[Scenes]/MainScene");

    }

    /*public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("Odaya Bağlanıldı");
        Debug.Log("Karakter Oluşturuluyor");
        PhotonNetwork.Instantiate("Player", new Vector3(-10, 5, -5), Quaternion.identity, 0, null);
    }*/

}
