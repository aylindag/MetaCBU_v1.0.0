using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;

public class PhotonFollowCamera : MonoBehaviour
{
    GameObject target = null;
    CinemachineVirtualCamera virtualCamera;
    private void Start()
    {
        virtualCamera = GetComponent<CinemachineVirtualCamera>();
    }
    void FixedUpdate()
    {
        //Sahnede kamera tanımlı ise kod çalışmadan kaldırılacak.
        if (target!= null)
        {
            Destroy(this);
        }
        try // Eğer kamera yoksa sahnede tag ile kamera aranacak. Bulunması durumunda target kısmına eşitlenecek.
        {
            target = GameObject.FindWithTag("CinemachineTarget");
            virtualCamera.Follow = target.transform;
        }
        catch (Exception) // Kamera bulunmazsa hata kodu yazdırılacak.
        {
            Debug.Log("Hedef Bulunamadı." );
        }
       
    }
}
