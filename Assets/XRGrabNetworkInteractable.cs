using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class XRGrabNetworkInteractable : XRGrabInteractable
{
    private PhotonView _photonView;
    // Start is called before the first frame update
    void Start()
    {
        _photonView = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    protected override void OnSelectEntered(XRBaseInteractor interactor)
    {
        _photonView.RequestOwnership();
        base.OnSelectEntered(interactor);
    }

    /*
    protected override void OnSelectEntering(XRBaseInteractor interactor)
    {
        _photonView.RequestOwnership();
        base.OnSelectEntering(interactor);
    }
    */
}
