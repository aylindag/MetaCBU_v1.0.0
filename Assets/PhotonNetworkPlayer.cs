using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Photon.Pun;
using StarterAssets;
using Unity.VideoHelper;
using Unity.VisualScripting;
using Unity.XR.CoreUtils;
using UnityEngine.XR.Interaction.Toolkit;

public class PhotonNetworkPlayer : MonoBehaviour
{

    public Transform head;
    public Transform leftHand;
    public Transform rightHand;
    private PhotonView _photonView;

    //public GameObject AvatarHeadGameObject;

    private Transform headRig;
    private Transform leftHandRig;
    private Transform rightHandRig;
    // Start is called before the first frame update
    void Start()
    {
        _photonView = GetComponent<PhotonView>();
        
        
        XROrigin xrOrigin = FindObjectOfType<XROrigin>();
        headRig= xrOrigin.transform.Find("Camera Offset/Main Camera/Head (1)");
        leftHandRig = xrOrigin.transform.Find("Camera Offset/Left Hand/LeftHand (1)");
        rightHandRig = xrOrigin.transform.Find("Camera Offset/Right Hand/RightHand (1)");
        
        

    }

    
    

    // Update is called once per frame
    void Update()
    {
        if (!_photonView.IsMine)
        {
           //rightHand.gameObject.SetActive(false);
           //leftHand.gameObject.SetActive(false);
           //head.gameObject.SetActive(false);
           //SetLayerRecursively(AvatarHeadGameObject , 0);
           
           
        }
        else
        {
            MapPosition(head , headRig);
            MapPosition(leftHand,leftHandRig);
            MapPosition(rightHand,rightHandRig);
            
            //SetLayerRecursively(AvatarHeadGameObject , 6);
        }
        
        
    }

    void MapPosition(Transform target, Transform xrOriginTransform)
    {
        target.position = xrOriginTransform.position;
        target.rotation = xrOriginTransform.rotation;
    }
    
  /*  void SetLayerRecursively(GameObject go, int layerNumber)
    {
        if (go == null) return;
        foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
        {
            trans.gameObject.layer = layerNumber;
        }
    }
    */
}
